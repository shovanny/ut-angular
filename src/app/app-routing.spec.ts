import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { APP_BASE_HREF, Location } from '@angular/common';
import { Router } from '@angular/router';
import { routes } from './app-routing.module';
import { UtComponentModule } from './ut-component/ut-component.module';
import { UtPipeModule } from './ut-pipe/ut-pipe.module';
import { UtDirectiveModule } from './ut-directive/ut-directive.module';
import { UtReactiveFormModule } from './ut-reactive-form/ut-reactive-form.module';
import { UtRoutingModule } from './ut-routing/ut-routing.module';

describe('app routing', () => {
  let router: Router;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        UtComponentModule,
        UtPipeModule,
        UtDirectiveModule,
        UtReactiveFormModule,
        UtRoutingModule
      ],
      providers: [ {
        provide: APP_BASE_HREF, useValue: '/dashboard'
      } ],
    }).compileComponents();
    router = TestBed.inject(Router);
    location = TestBed.inject(Location)
    router.initialNavigation();
  });

  it('should be route ut-component', fakeAsync(() => {
    router.navigateByUrl('/dashboard/ut-component');
    tick(50);
    expect(location.path()).toBe('/dashboard/ut-component');
  }));

  it('should be route ut-pipe', fakeAsync(() => {
    router.navigateByUrl('/dashboard/ut-pipe');
    tick(50);
    expect(location.path()).toBe('/dashboard/ut-pipe');
  }));

  it('should be route ut-directive', fakeAsync(() => {
    router.navigateByUrl('/dashboard/ut-directive');
    tick(50);
    expect(location.path()).toBe('/dashboard/ut-directive');
  }));

  it('should be route ut-reactive-form', fakeAsync(() => {
    router.navigateByUrl('/dashboard/ut-reactive-form');
    tick(50);
    expect(location.path()).toBe('/dashboard/ut-reactive-form');
  }));

  it('should be route ut-routing', fakeAsync(() => {
    router.navigateByUrl('/dashboard/ut-routing');
    tick(50);
    expect(location.path()).toBe('/dashboard/ut-routing');
  }));
});

