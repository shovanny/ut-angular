import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtRLazyLoadingComponent } from './ut-r-lazy-loading.component';

describe('UtRLazyLoadingComponent', () => {
  let component: UtRLazyLoadingComponent;
  let fixture: ComponentFixture<UtRLazyLoadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtRLazyLoadingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtRLazyLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
