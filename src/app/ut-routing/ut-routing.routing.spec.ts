import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { routes } from './ut-routing.routing';

describe('ut routing', () => {
  let router: Router;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes)
      ]
    }).compileComponents();
    router = TestBed.inject(Router);
    location = TestBed.inject(Location)
    router.initialNavigation();
  });

  it('should be route simple', fakeAsync(() => {
    router.navigateByUrl('/simple');
    tick(50);
    expect(location.path()).toBe('/simple');
  }));
  it('should be route lazy', fakeAsync(() => {
    router.navigateByUrl('/lazy');
    tick(50);
    expect(location.path()).toBe('/lazy');
  }));
});

