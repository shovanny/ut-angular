import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtRSimpleComponent } from './ut-r-simple.component';

describe('UtRSimpleComponent', () => {
  let component: UtRSimpleComponent;
  let fixture: ComponentFixture<UtRSimpleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtRSimpleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtRSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
