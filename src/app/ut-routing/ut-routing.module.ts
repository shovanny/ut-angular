import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtRRoutingModule } from './ut-routing.routing';
import { UtRLazyLoadingComponent } from './ut-r-lazy-loading/ut-r-lazy-loading.component';
import { UtRSimpleComponent } from './ut-r-simple/ut-r-simple.component';

@NgModule({
  declarations: [
    UtRLazyLoadingComponent,
    UtRSimpleComponent
  ],
  imports: [
    CommonModule,
    UtRRoutingModule
  ]
})
export class UtRoutingModule { }
