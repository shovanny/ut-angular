import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UtRLazyLoadingComponent } from './ut-r-lazy-loading/ut-r-lazy-loading.component';
import { UtRSimpleComponent } from './ut-r-simple/ut-r-simple.component';

export const routes: Routes = [
  {
    path: 'simple', component: UtRSimpleComponent
  },
  {
    path: 'lazy', component: UtRLazyLoadingComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class UtRRoutingModule {
}
