import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cmp-children',
  templateUrl: './cmp-children.component.html',
  styleUrls: [ './cmp-children.component.scss' ]
})
export class CmpChildrenComponent implements OnInit {
  @Input() yearsOld!: number;
  @Output() outPutMajor: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit(): void {}

  /**
   * docs
   */
  isMajor = (): void => {
    this.outPutMajor.emit(this.yearsOld >= 18);
  }

}
