import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CmpChildrenComponent } from './cmp-children.component';

describe('CmpChildrenComponent', () => {
  let component: CmpChildrenComponent;
  let fixture: ComponentFixture<CmpChildrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CmpChildrenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CmpChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
