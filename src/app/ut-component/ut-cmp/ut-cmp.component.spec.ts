import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtCmpComponent } from './ut-cmp.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CmpChildrenComponent } from '../cmp-children/cmp-children.component';

describe('UtComponentComponent', () => {
  let component: UtCmpComponent;
  let fixture: ComponentFixture<UtCmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtCmpComponent, CmpChildrenComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtCmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('Testing Input in Ut Component', () => {
  let component: UtCmpComponent;
  let fixture: ComponentFixture<UtCmpComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        UtCmpComponent, CmpChildrenComponent
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
    fixture = TestBed.createComponent(UtCmpComponent);
    component = fixture.componentInstance;
  });

  it('should yearsOld is major', () => {
    const button: HTMLButtonElement = fixture.nativeElement.querySelector('#isMajor');
    const spy = spyOn(component, 'verifiedYearsOld');
    component.yearsOld = 18;
    fixture.detectChanges();
    button.click();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith(true);
  });

  it('should yearsOld is minor', () => {
    const button: HTMLButtonElement = fixture.nativeElement.querySelector('#isMajor');
    const spy = spyOn(component, 'verifiedYearsOld');
    component.yearsOld = 15;
    fixture.detectChanges();
    button.click();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith(false);
  });
});

describe('Testing Output in Ut Component', () => {
  let component: UtCmpComponent;
  let fixture: ComponentFixture<UtCmpComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        UtCmpComponent, CmpChildrenComponent
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
    fixture = TestBed.createComponent(UtCmpComponent);
    component = fixture.componentInstance;
  });

  it('should be emit and receive method for cmp-children when is major', () => {
    const button: HTMLButtonElement = fixture.nativeElement.querySelector('#isMajor');
    component.yearsOld = 18;
    fixture.detectChanges();
    button.click();
    fixture.detectChanges();
    expect(component.message).toEqual('Eres mayor de edad');
  });

  it('should be emit and receive method for cmp-children when is minor', () => {
    const button: HTMLButtonElement = fixture.nativeElement.querySelector('#isMajor');
    component.yearsOld = 15;
    fixture.detectChanges();
    button.click();
    fixture.detectChanges();
    expect(component.message).toEqual('Eres menor de edad');
  });
});
