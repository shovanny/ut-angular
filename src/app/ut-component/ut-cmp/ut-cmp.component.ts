import { Component, OnInit } from '@angular/core';
import { topicModel } from '../../shared/models/concepts.model';

@Component({
  selector: 'app-ut-component',
  templateUrl: './ut-cmp.component.html',
  styleUrls: [ './ut-cmp.component.scss' ]
})
export class UtCmpComponent implements OnInit {
  message!: string;
  yearsOld!: number;
  concepts!: topicModel;
  idSelected!: string;
  isActive!: boolean;

  constructor() {
    this.idSelected = '';
  }

  ngOnInit(): void {
    this.initComponent();
  }

  /**
   * docs
   * @param yearsOld docs
   */
  verifiedYearsOld = (yearsOld: boolean) => {
    this.message = yearsOld ? 'Eres mayor de edad' : 'Eres menor de edad';
  }

  /**
   * docs
   */
  initComponent = (): void => {
    this.concepts = {
      theory: {
        id: '01',
        classIsActive: false
      },
      practice: {
        id: '02',
        classIsActive: true
      }
    }
  }

  /**
   * docs
   */
  step = (id: string): void => {
    this.idSelected = id;
    this.isActive = true;
  }
}
