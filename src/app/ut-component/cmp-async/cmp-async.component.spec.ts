import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { CmpAsyncComponent } from './cmp-async.component';

describe('CmpAsyncComponent', () => {
  let component: CmpAsyncComponent;
  let fixture: ComponentFixture<CmpAsyncComponent>;

  /*beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CmpAsyncComponent ]
    })
      .compileComponents();
  });*/

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ CmpAsyncComponent ]
    }).compileComponents();
    fixture = TestBed.createComponent(CmpAsyncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get data with async/pipe', async () => {
    // fixture.whenStable().then(() => {
    //   fixture.detectChanges();
    //   expect(fixture.nativeElement.querySelectorAll('p').length).toBe(3);
    // });
    await fixture.whenStable();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelectorAll('p').length).toBe(3);
  });

  it('should get data salary with fakeAsync', fakeAsync(() => {
    component.loadSalary();
    tick(1500);
    expect(component.salary).toBe(3000);
    tick(2000);
    expect(component.salary).toBe(9000);
    tick(4000);
    expect(component.salary).toBe(12000);
  }));
});
