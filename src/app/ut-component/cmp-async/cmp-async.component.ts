import { Component, OnInit } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { AsyncService } from '../../shared/service/async.service';

@Component({
  selector: 'app-cmp-async',
  templateUrl: './cmp-async.component.html',
  styleUrls: [ './cmp-async.component.scss' ]
})
export class CmpAsyncComponent implements OnInit {
  data$!: Observable<string[]>;
  salary!: number;

  constructor(
    private readonly asyncService: AsyncService
  ) {
  }

  ngOnInit(): void {
    this.initComponent();
  }

  /**
   * docs
   */
  initComponent = (): void => {
    this.data$ = this.loadHeroes();
  }

  /**
   * docs
   */
  loadHeroes = (): Observable<string[]> => {
    return this.asyncService.getData();
  }

  /**
   * docs
   */
  loadSalary = (): void => {
    let salary = 1000;

    setTimeout(() => {
      salary = 3000;
      this.salary = salary;
    }, 1000);

    setTimeout(() => {
      salary = 9000;
      this.salary = salary;
    }, 2000);

    timer(4000).subscribe(() => {
      this.salary = 12000;
    })

  }
}
