import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpSetGetComponent } from './http-set-get/http-set-get.component';
import { KarmaComponent } from './karma/karma.component';
import { SonarqubeComponent } from './sonarqube/sonarqube.component';
import { UtCmpComponent } from './ut-cmp/ut-cmp.component';

const routes: Routes = [
  {
    path: '', component: UtCmpComponent
  },
  {
    path: 'http-set-get', component: HttpSetGetComponent
  },
  {
    path: 'sonar', component: SonarqubeComponent
  },
  {
    path: 'karma', component: KarmaComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class UtComponentRoutingModule {
}
