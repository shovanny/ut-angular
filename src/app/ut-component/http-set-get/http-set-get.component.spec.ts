import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpSetGetComponent } from './http-set-get.component';

describe('HttpSetGetComponent', () => {
  let component: HttpSetGetComponent;
  let fixture: ComponentFixture<HttpSetGetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HttpSetGetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpSetGetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
