import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CmpSpyComponent } from './cmp-spy.component';
import { Title } from '@angular/platform-browser';

describe('CmpSpyComponent', () => {
  let component: CmpSpyComponent;
  let fixture: ComponentFixture<CmpSpyComponent>;
  let titleService: Title;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CmpSpyComponent ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CmpSpyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'Prueba unitaria simple con spy'`, () => {
    titleService = TestBed.inject(Title);
    const spy = spyOn(titleService, 'setTitle');
    component.initTitle();
    //console.log(`${spy.calls.mostRecent().args[0]}`);
    expect(spy.calls.mostRecent().args[0]).toBe('Prueba unitaria simple con spy');
    expect(spy).toHaveBeenCalled();
    //expect(spy).toHaveBeenCalledWith(spy.calls.mostRecent().args[0]);
  });

  it('should render cmp-spy works!', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('p').textContent).not.toContain('mi texto');
  });
});
