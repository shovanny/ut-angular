import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-cmp-spy',
  templateUrl: './cmp-spy.component.html',
  styleUrls: ['./cmp-spy.component.scss']
})
export class CmpSpyComponent implements OnInit {
  caption!: string;
  constructor(
    private readonly title: Title
  ) { }

  ngOnInit(): void {
    this.initTitle();
  }

  initTitle = () => {
    this.title.setTitle('Prueba unitaria simple con spy');
    this.caption = this.title.getTitle();
  }

  proccessTitle = (): string => {
    return this.caption + ' hola';
  }
}
