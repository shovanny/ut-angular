import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AsyncService } from '../../shared/service/async.service';

@Component({
  selector: 'app-cmp-spy-jasmine',
  templateUrl: './cmp-spy-jasmine.component.html',
  styleUrls: ['./cmp-spy-jasmine.component.scss']
})
export class CmpSpyJasmineComponent implements OnInit {
  caption!: string;
  myData!: string[];

  constructor(
    private readonly title: Title,
    private readonly data: AsyncService
  ) { }

  ngOnInit(): void {
    this.initComponent();
  }

  initComponent() {
    this.title.setTitle('spy usando createSpyObj');
    this.caption = this.title.getTitle();

    this.data.getData().subscribe((data) => {
      this.myData = data;
    });
  }
}
