import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CmpSpyJasmineComponent } from './cmp-spy-jasmine.component';
import { Title } from '@angular/platform-browser';
import { AsyncService } from '../../shared/service/async.service';
import { of } from 'rxjs';


describe('CmpSpyJasmineComponent', () => {
  let component: CmpSpyJasmineComponent;
  let fixture: ComponentFixture<CmpSpyJasmineComponent>;
  let titleSpy: jasmine.SpyObj<Title>;
  let asyncService: jasmine.SpyObj<AsyncService>;
  const myDataAHeroes = [ 'heidi', 'arale', 'don gato' ];

  function configurationSpy() {
    titleSpy = jasmine.createSpyObj('Title', [ 'getTitle', 'setTitle' ]);
    titleSpy.getTitle.and.returnValue('My title');
    asyncService = jasmine.createSpyObj('AsyncService', [ 'getData' ]);
    asyncService.getData.and.returnValue(of(myDataAHeroes));
  }

  beforeEach(async () => {
    configurationSpy();
    await TestBed.configureTestingModule({
      declarations: [ CmpSpyJasmineComponent ],
      providers: [
        {
          provide: Title, useValue: titleSpy
        },
        {
          provide: AsyncService, useValue: asyncService
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CmpSpyJasmineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call method initComponent with title', () => {
    fixture.detectChanges();
    // console.log(fixture.nativeElement.querySelector('span').textContent);
    // console.log('dsadadasdasd', component.myData);
    expect(fixture.nativeElement.querySelector('span').textContent).toEqual('My title');
    expect(component.myData.length).toBe(3);
  });
});
