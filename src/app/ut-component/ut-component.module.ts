import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtComponentRoutingModule } from './ut-component-routing.module';
import { UtCmpComponent } from './ut-cmp/ut-cmp.component';
import { CmpSpyComponent } from './cmp-spy/cmp-spy.component';
import { CmpSpyJasmineComponent } from './cmp-spy-jasmine/cmp-spy-jasmine.component';
import { CmpAsyncComponent } from './cmp-async/cmp-async.component';
import { CmpChildrenComponent } from './cmp-children/cmp-children.component';
import { HttpSetGetComponent } from './http-set-get/http-set-get.component';
import { KarmaComponent } from './karma/karma.component';
import { SonarqubeComponent } from './sonarqube/sonarqube.component';


@NgModule({
  declarations: [
    UtCmpComponent,
    CmpSpyComponent,
    CmpSpyJasmineComponent,
    CmpAsyncComponent,
    CmpChildrenComponent,
    HttpSetGetComponent,
    KarmaComponent,
    SonarqubeComponent
  ],
  imports: [
    CommonModule,
    UtComponentRoutingModule
  ],
})
export class UtComponentModule { }
