import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appCopyright]'
})
export class CopyrightDirective {
  myNumber!: number;

  /**
   * docs
   * @param el docs
   * @param renderer docs
   */
  constructor(
    private readonly el: ElementRef,
    private readonly renderer: Renderer2
  ) {
    renderer.addClass(el.nativeElement, 'copyright');
    renderer.setProperty(
      el.nativeElement,
      'textContent',
      `Copyright ©${new Date().getFullYear()} All Rights Reserved.`
    );
  }

  @HostListener('mouseenter')
  onMouseEnter(): void {
    this.processColor();
    this.el.nativeElement.style.backgroundColor = 'orange';
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.processColor();
    this.el.nativeElement.style.backgroundColor = '';
  }

  /**
   * docs
   * @param param docs
   */
  processColor = (): string => {
    let color = '';
    const value = Number((Math.random() * 10).toFixed(0));
    if ((value % 2) === 0) {
      color = 'orange';
    } else {
      color = 'yellow';
    }
    return color;
  }
}
