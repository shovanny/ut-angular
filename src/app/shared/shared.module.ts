import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { ListPipe } from './pipes/list.pipe';
import { CopyrightDirective } from './directives/copyright.directive';



@NgModule({
  declarations: [
    FooterComponent,
    ListPipe,
    CopyrightDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FooterComponent,
    ListPipe,
    CopyrightDirective
  ],
})
export class SharedModule { }
