import { TestBed } from '@angular/core/testing';

import { AsyncService } from './async.service';

describe('AsyncServiceService', () => {
  let service: AsyncService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AsyncService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
