import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AsyncService {

  constructor() {
  }

  getData(): Observable<string[]> {
    const heroes = [ 'batman', 'superman', 'wolverine' ];
    return of(heroes).pipe(delay(500));
  }
}
