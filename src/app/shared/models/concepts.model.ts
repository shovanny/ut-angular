export interface ConceptModel {
  remember: ContentConceptModel;
  begin: ContentConceptModel;
  anatomy: ContentConceptModel;
}

export interface topicModel {
  theory: ContentConceptModel;
  practice: ContentConceptModel;
}

interface ContentConceptModel {
  id: string;
  classIsActive?: boolean;
}
