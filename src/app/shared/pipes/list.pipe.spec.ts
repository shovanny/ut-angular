import { ListPipe } from './list.pipe';

describe('ListPipe', () => {
  const pipe = new ListPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it(`should have is 'Hol_'`, () => {
    expect(pipe.transform('Hola')).toEqual('Hol_');
  });

  it(`should have is 'na'`, () => {
    expect(pipe.transform('')).toEqual('na');
  });
});
