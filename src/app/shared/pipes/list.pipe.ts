import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'list'
})
export class ListPipe implements PipeTransform {

  transform(value: string): string {
    let message;
    if (!value) {
      message = 'na';
    } else {
      message = value.replace(/a/g, '_');
    }
    return message;
  }
}
