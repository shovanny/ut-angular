/**
 * Default actions by form
 */
export interface ActionFormInterface {
  /**
   * docs
   */
  save(): void;

  /**
   * docs
   */
  update?(): void;

  /**
   * docs
   */
  delete?(): void;
}
