import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home/home.component';
import { RememberComponent } from './remember/remember.component';
import { BeginComponent } from './begin/begin.component';
import { AnatomyComponent } from './anatomy/anatomy.component';


@NgModule({
  declarations: [
    HomeComponent,
    RememberComponent,
    BeginComponent,
    AnatomyComponent
  ],
  imports: [
    CommonModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class HomeModule {
}
