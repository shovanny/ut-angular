import { Component, OnInit } from '@angular/core';
import { ConceptModel } from '../../shared/models/concepts.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.scss' ]
})
export class HomeComponent implements OnInit {
  concepts!: ConceptModel;
  idSelected!: string;
  isActive!: boolean;

  constructor() {
    this.idSelected = '';
  }

  ngOnInit(): void {
    this.initComponent();
  }

  /**
   * docs
   */
  initComponent = (): void => {
    this.concepts = {
      remember: {
        id: '01',
        classIsActive: false
      },
      begin: {
        id: '02',
        classIsActive: true
      },
      anatomy: {
        id: '03',
        classIsActive: true
      }
    }
  }

  /**
   * docs
   */
  step = (id: string): void => {
    this.idSelected = id;
    this.isActive = true;
  }
}
