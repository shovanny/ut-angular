import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home/home.component';

export const routes: Routes = [
  {
    path: 'dashboard',
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'ut-component',
        loadChildren: () => import('./ut-component/ut-component.module').then(m => m.UtComponentModule)
      },
      {
        path: 'ut-pipe',
        loadChildren: () => import('./ut-pipe/ut-pipe.module').then(m => m.UtPipeModule)
      },
      {
        path: 'ut-directive',
        loadChildren: () => import('./ut-directive/ut-directive.module').then(m => m.UtDirectiveModule)
      },
      {
        path: 'ut-reactive-form',
        loadChildren: () => import('./ut-reactive-form/ut-reactive-form.module').then(m => m.UtReactiveFormModule)
      },
      {
        path: 'ut-routing',
        loadChildren: () => import('./ut-routing/ut-routing.module').then(m => m.UtRoutingModule)
      },
      {
        path: '**',
        redirectTo: 'home'
      }
    ],
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard'
  },
  {
    path: '**',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
