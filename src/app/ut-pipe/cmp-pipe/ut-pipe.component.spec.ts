import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtPipeComponent } from './ut-pipe.component';
import { SharedModule } from '../../shared/shared.module';

describe('UtPipeComponent', () => {
  let component: UtPipeComponent;
  let fixture: ComponentFixture<UtPipeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtPipeComponent ],
      imports: [SharedModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtPipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
