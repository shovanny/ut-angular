import { Component, OnInit } from '@angular/core';
import { topicModel } from '../../shared/models/concepts.model';

@Component({
  selector: 'app-ut-pipe',
  templateUrl: './ut-pipe.component.html',
  styleUrls: ['./ut-pipe.component.scss']
})
export class UtPipeComponent implements OnInit {
  message!: string;
  concepts!: topicModel;
  idSelected!: string;
  isActive!: boolean;

  constructor() {
    this.idSelected = '';
  }

  ngOnInit(): void {
    this.message = 'Esta es una prueba utilizando pipes';
    this.initComponent();
  }

  /**
   * docs
   */
  initComponent = (): void => {
    this.concepts = {
      theory: {
        id: '01',
        classIsActive: false
      },
      practice: {
        id: '02',
        classIsActive: true
      }
    }
  }

  /**
   * docs
   */
  step = (id: string): void => {
    this.idSelected = id;
    this.isActive = true;
  }
}
