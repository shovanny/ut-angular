import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtPipeRoutingModule } from './ut-pipe-routing.module';
import { UtPipeComponent } from './cmp-pipe/ut-pipe.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    UtPipeComponent
  ],
  imports: [
    CommonModule,
    UtPipeRoutingModule,
    SharedModule
  ]
})
export class UtPipeModule { }
