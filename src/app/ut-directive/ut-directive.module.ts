import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtDirectiveRoutingModule } from './ut-directive-routing.module';
import { UtDirectiveComponent } from './cmp-directive/ut-directive.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    UtDirectiveComponent
  ],
  imports: [
    CommonModule,
    UtDirectiveRoutingModule,
    SharedModule
  ]
})
export class UtDirectiveModule {
}
