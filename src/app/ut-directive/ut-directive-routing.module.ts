import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UtDirectiveComponent } from './cmp-directive/ut-directive.component';

const routes: Routes = [
  {
    path: '',
    component: UtDirectiveComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class UtDirectiveRoutingModule {
}
