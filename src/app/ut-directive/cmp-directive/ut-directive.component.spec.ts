import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtDirectiveComponent } from './ut-directive.component';

describe('UtDirectiveComponent', () => {
  let component: UtDirectiveComponent;
  let fixture: ComponentFixture<UtDirectiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtDirectiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtDirectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
