import { Component, OnInit } from '@angular/core';
import { topicModel } from '../../shared/models/concepts.model';

@Component({
  selector: 'app-ut-directive',
  templateUrl: './ut-directive.component.html',
  styleUrls: [ './ut-directive.component.scss' ]
})
export class UtDirectiveComponent implements OnInit {
  concepts!: topicModel;
  idSelected!: string;
  isActive!: boolean;

  constructor() {
    this.idSelected = '';
  }

  ngOnInit(): void {
    this.initComponent();
  }

  /**
   * docs
   */
  initComponent = (): void => {
    this.concepts = {
      theory: {
        id: '01',
        classIsActive: false
      },
      practice: {
        id: '02',
        classIsActive: true
      }
    }
  }

  /**
   * docs
   */
  step = (id: string): void => {
    this.idSelected = id;
    this.isActive = true;
  }

}
