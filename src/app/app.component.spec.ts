import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('Suite 1', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent,
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});

describe('Suite 2', () => {
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedModule,
        CoreModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  it(`should have as title ':)'`, () => {
    expect(app.title).toEqual(':)');
  });

  it(`should have as title ':('`, () => {
    expect(app.title).not.toEqual(':(');
  });
});

describe('Suite 3', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedModule,
        CoreModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    // expect(compiled.querySelector('h1').textContent).toContain(':)');
    console.log(compiled.querySelector('app-header').textContent);
    expect(compiled.querySelector('app-header').textContent).not.toBeNull();
  });
});

/*describe('Name for suite', () => {
  let total = 0;
  beforeEach(() => total = 1);

  it('should be 2', () => {
    total+=1;
    expect(total).toBe(2);
  });

  it('should be 0', () => {
    total-=1;
    expect(total).toBe(0);
  });

  afterEach(() => total = 0);
});*/
