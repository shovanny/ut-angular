import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActionFormInterface } from '../../shared/utils/action-form.interface';
import { topicModel } from '../../shared/models/concepts.model';

@Component({
  selector: 'app-ut-reactive-form',
  templateUrl: './ut-reactive-form.component.html',
  styleUrls: [ './ut-reactive-form.component.scss' ]
})
export class UtReactiveFormComponent implements OnInit, ActionFormInterface {
  form!: FormGroup;
  concepts!: topicModel;
  idSelected!: string;
  isActive!: boolean;

  constructor(
    private readonly fb: FormBuilder,
  ) {
    this.idSelected = '';
  }

  ngOnInit(): void {
    this.initComponent();
  }

  /**
   * docs
   * @param param docs
   */
  initComponent = (): void => {
    this.form = this.fb.group({
      searchText: [ '', Validators.required ]
    })
    this.concepts = {
      theory: {
        id: '01',
        classIsActive: false
      },
      practice: {
        id: '02',
        classIsActive: true
      }
    }
  }

  /**
   * Override docs
   */
  save(): void {
    console.log(`Los datos son: ${this.searchText}`);
  }

  /**
   * docs
   */
  step = (id: string): void => {
    this.idSelected = id;
    this.isActive = true;
  }

  get searchText(): string {
    return this.form.get('searchText')!.value;
  }

  set searchText(value: string) {
    this.form.get('searchText')!.setValue(value);
  }
}
