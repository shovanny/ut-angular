import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtReactiveFormComponent } from './ut-reactive-form.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('UtReactiveFormComponent', () => {
  let component: UtReactiveFormComponent;
  let fixture: ComponentFixture<UtReactiveFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        UtReactiveFormComponent
      ],
      imports: [
        ReactiveFormsModule
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtReactiveFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have Angular value', () => {
    const input: HTMLInputElement = fixture.nativeElement.querySelector('input');
    input.value = 'Angular';
    input.dispatchEvent(new Event('input'));
    expect(component.searchText).toBe('Angular');
  });

  it('should have Angular value usgin setter', () => {
    component.searchText = 'Angular';
    expect(component.searchText).toBe('Angular');
  });

  it('should be call console', () => {
    const button: HTMLButtonElement = fixture.nativeElement.querySelector('#submitForm');
    const spy = spyOn(console, 'log');
    component.searchText = 'Angular';
    fixture.detectChanges();
    button.click();
    // expect(component.searchText).toBe('Angular');
    // expect(spy).toHaveBeenCalled();
    // expect(spy).toHaveBeenCalledWith('Los datos son: Angular');
    expect(spy.calls.first().args[0]).toContain('Angular');
  });
});
