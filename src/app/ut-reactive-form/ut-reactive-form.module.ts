import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtReactiveFormRoutingModule } from './ut-reactive-form-routing.module';
import { UtReactiveFormComponent } from './cmp-reactive-form/ut-reactive-form.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UtReactiveFormComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UtReactiveFormRoutingModule
  ]
})
export class UtReactiveFormModule { }
