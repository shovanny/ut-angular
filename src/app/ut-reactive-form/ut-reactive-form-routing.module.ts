import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UtReactiveFormComponent } from './cmp-reactive-form/ut-reactive-form.component';

const routes: Routes = [
  {
    path: '', component: UtReactiveFormComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class UtReactiveFormRoutingModule {
}
